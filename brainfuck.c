#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    if (argc < 2) exit(0);

    FILE *program;
    program = fopen(argv[1], "r");
    
    unsigned char m[30000] = {0};
    unsigned char* ptr = m;
    int loop;

    char op;

    while ((op = fgetc(program)) != EOF) {
        switch (op) {
        case '<': {
            --ptr;
            break;
        }
        case '>': {
            ++ptr;
            break;
        } 
        case '+': {
            ++*ptr;
            break;
        }
        case '-': {
            --*ptr;
            break;
        }
        case '.': {
            putchar(*ptr);
            break;
        }
        case ',': {
            *ptr = getchar();
            break;
        }
        case '[': {
            if (*ptr == 0) {
                loop = 1;
                while (loop > 0) {
                    op = fgetc(program);
                    if (op == '[') ++loop;
                    else if (op == ']') --loop;
                }
            }
            break;
        }
        case ']': {
            if (*ptr != 0) {
                loop = 1;
                while (loop > 0) {
                    fseek(program, -2, SEEK_CUR);
                    op = fgetc(program);
                    if (op == '[') --loop;
                    else if (op == ']') ++loop;
                }
            }
	    break;
        }
        }
    }
    return 0;
}
